import * as Assert from "assert";
import {it} from "mocha";
import {ParticleEmitter, ParticleSystem} from "../src";
import {ParticleContainer, Ticker} from "pixi.js";
import {ParticleDebugCounterModule} from "../src/modules/debug";
import MockParticleTickTrackerModule from "./mock/MockParticleTickTrackerModule";

describe("ParticleEmitter", ()=>
{
	let ticker: Ticker;
	let particleSystem: ParticleSystem;
	let emitter: ParticleEmitter;
	let particleCounter: ParticleDebugCounterModule;

	beforeEach(()=>
	{
		ticker = new Ticker();
		particleSystem = new ParticleSystem(ticker);
		emitter = new ParticleEmitter(ticker, particleSystem);
		particleCounter = new ParticleDebugCounterModule();

		emitter.define(particleCounter);
		ticker.update(0);
	});

	context("When using `emitBatch`", ()=>
	{
		it("the number of particles created is defined by the set batchCount.", ()=>
		{
			emitter.batchCount(10);

			Assert.strictEqual(particleCounter.particleCount, 0);
			emitter.emitBatch();
			Assert.strictEqual(particleCounter.particleCount, 10);
			emitter.batchCount(3);
			Assert.strictEqual(particleCounter.particleCount, 10);
			emitter.emitBatch();
			Assert.strictEqual(particleCounter.particleCount, 13);
		});
	});

	context("When running an emitter", ()=>
	{
		it("the particles only start spawning after the `emitRate` has elapsed.", ()=>
		{
			emitter.batchCount(1).emitRate(10);
			emitter.start();
			Assert.strictEqual(particleCounter.particleCount, 0);

			ticker.update(10);
			Assert.strictEqual(particleCounter.particleCount, 1);
		});

		it("the particles keep spawning each time the `emitRate` is elapsed.", ()=>
		{
			emitter.batchCount(1).emitRate(10);
			emitter.start();

			for(let i=0; i<100; i++)
			{
				ticker.update(10 * i);
				Assert.strictEqual(particleCounter.particleCount, i);
			}
		});

		it("a batch is spawned for each time the `emitRate` is elapsed in one tick.", ()=>
		{
			emitter.batchCount(1).emitRate(10);
			emitter.start();

			ticker.update(100);
			Assert.strictEqual(particleCounter.particleCount, 10);
		});

		it("a batch is spawned for each time the `emitRate` is elapsed in one tick.", ()=>
		{
			emitter.batchCount(1).emitRate(10);
			emitter.start();

			ticker.update(100);
			Assert.strictEqual(particleCounter.particleCount, 10);
		});

		it("particles will spawn till the emitter is stopped.", ()=>
		{
			emitter.batchCount(1).emitRate(1);
			emitter.start();

			ticker.update(1);
			Assert.strictEqual(particleCounter.particleCount, 1);

			ticker.update(2);
			Assert.strictEqual(particleCounter.particleCount, 2);

			emitter.stop();
			ticker.update(3);
			Assert.strictEqual(particleCounter.particleCount, 2);
		});
	});


	context("When interpolating a tick between batches", ()=>
	{
		it("each batch of particles is updated based on the intended elapsed time for that batch.", ()=>
		{
			const tickTracker = new MockParticleTickTrackerModule();

			emitter
				.define(tickTracker)
				.batchCount(1)
				.emitRate(10)
				.start();

			ticker.update(10);
			Assert.strictEqual(tickTracker.getLastTracker()?.ticked, 0);

			ticker.update(15);
			Assert.strictEqual(tickTracker.getLastTracker()?.ticked, 5);

			ticker.update(22);
			Assert.strictEqual(tickTracker.getLastTracker()?.ticked, 2);
		});
	});

	context("When setting a container", ()=>
	{
		it("spawned particles are added to the container.", ()=>
		{
			const container = new ParticleContainer();
			emitter
				.container(container)
				.batchCount(1);

			emitter.emitBatch();
			emitter.emitBatch();
			emitter.emitBatch();

			Assert.strictEqual(container.children.length, 3);
		});
	});
});