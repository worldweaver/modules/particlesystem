import {IParticle, IParticleComponent, IParticleModule} from "../../src";
import MockDisposableParticleComponent from "./MockDisposableParticleComponent";

export default class MockDisposableParticleModule implements IParticleModule
{
	public lastComponent: MockDisposableParticleComponent | undefined;

	createComponent(particle: IParticle): IParticleComponent
	{
		this.lastComponent = new MockDisposableParticleComponent();
		return this.lastComponent;
	}
}