import {IParticle, IParticleComponent} from "../../src";

export default class MockDisposableParticleComponent implements IParticleComponent
{
	public disposed: boolean = false;

	dispose(): void
	{
		this.disposed = true;
	}

	init(particle: IParticle): void
	{
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
	}

}