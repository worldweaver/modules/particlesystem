import {IParticle, IParticleComponent, IParticleModule} from "../../src";
import MockParticleTickTracker from "./MockParticleTickTracker";

export default class MockParticleTickTrackerModule implements IParticleModule
{
	private _lastTracker: MockParticleTickTracker | undefined;

	public getLastTracker() : MockParticleTickTracker | undefined
	{
		return this._lastTracker;
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		this._lastTracker = new MockParticleTickTracker();
		return this._lastTracker;
	}
}