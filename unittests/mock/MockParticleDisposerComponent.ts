import {IParticle, IParticleComponent} from "../../src";

export default class MockParticleDisposerComponent implements IParticleComponent
{
	init(particle: IParticle): void
	{
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
	}

	dispose(): void
	{
	}
}