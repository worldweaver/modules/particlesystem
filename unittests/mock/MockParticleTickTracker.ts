import {IParticle, IParticleComponent} from "../../src";

export default class MockParticleTickTracker implements IParticleComponent
{
	private _ticked: number = 0;
	get ticked() : number
	{
		return this._ticked;
	}

	init(particle: IParticle): void
	{
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
		this._ticked += elapsedTime;
	}

	dispose(): void
	{
	}
}