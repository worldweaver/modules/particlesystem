import {IParticle, IParticleComponent, IParticleModule} from "../../src";
import MockParticleDisposerComponent from "./MockParticleDisposerComponent";

export default class MockParticleDisposer implements IParticleModule
{
	private _particles: Array<IParticle>;

	constructor()
	{
		this._particles = new Array<IParticle>();
	}

	public disposeParticle(): void
	{
		this._particles.pop()?.dispose();
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		this._particles.push(particle);
		return new MockParticleDisposerComponent();
	}
}