import {ParticleContainer, Ticker} from "pixi.js";
import {
	Particle,
	ParticleEmitter,
	ParticleLifeSpanComponent,
	ParticleLifeSpanModule,
	ParticleSystem
} from "../../src";
import MockDisposableParticleModule from "../mock/MockDisposableParticleModule";
import * as Assert from "assert";
import {it} from "mocha";
import TestUtils from "../TestUtils";

describe("LifeSpanModule", ()=>
{
	let ticker: Ticker;
	let particleSystem: ParticleSystem;
	let emitter: ParticleEmitter;
	let lifeSpanModule: ParticleLifeSpanModule;

	beforeEach(()=>
	{
		ticker = new Ticker();
		particleSystem = new ParticleSystem(ticker);
		emitter = new ParticleEmitter(ticker, particleSystem);
		lifeSpanModule = new ParticleLifeSpanModule();

		emitter.define(lifeSpanModule);
		ticker.update(0);
	});

	context("When giving a particle a lifespan", ()=>
	{
		it("the particle is disposed when the given timeframe expires.", ()=>
		{
			lifeSpanModule.lifespan(10);
			const disposableModule = new MockDisposableParticleModule();
			emitter.define(disposableModule);

			emitter.emitParticle();

			Assert.strictEqual(disposableModule.lastComponent?.disposed, false);

			ticker.update(10);

			Assert.strictEqual(disposableModule.lastComponent?.disposed, true);
		});

		it("the size is selected from the given range at random.", ()=>
		{
			lifeSpanModule.lifespanRange(0, 10);

			TestUtils.fakeRandomReturnValues(0, 0.5, 0.9);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleLifeSpanComponent>particle1.components[0]).lifeSpan, 0);

			const particle2 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleLifeSpanComponent>particle2.components[0]).lifeSpan, 5);

			const particle3 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleLifeSpanComponent>particle3.components[0]).lifeSpan, 9);
		});
	});

	context("When a particle lifespan expires", ()=>
	{
		it("it is removed from its container.", ()=>
		{
			const container = new ParticleContainer();
			emitter.container(container);
			lifeSpanModule.lifespan(5);

			emitter.emitParticle();

			Assert.strictEqual(container.children.length, 1);

			ticker.update(10);

			Assert.strictEqual(container.children.length, 0);
		});
	});
})