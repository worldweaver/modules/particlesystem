import {Ticker} from "pixi.js";
import {Particle, ParticleEmitter, ParticleMovementComponent, ParticleMovementModule, ParticleSystem} from "../../src";
import {it} from "mocha";
import Vector2SpawnRegion from "../../src/regions/Vector2SpawnRegion";
import {Vector2} from "@worldweaver/math";
import * as Assert from "assert";
import TestUtils from "../TestUtils";

describe("ParticleMovementModule", ()=>
{
	let ticker: Ticker;
	let particleSystem: ParticleSystem;
	let emitter: ParticleEmitter;
	let movementModule: ParticleMovementModule;

	beforeEach(()=>
	{
		ticker = new Ticker();
		particleSystem = new ParticleSystem(ticker);
		emitter = new ParticleEmitter(ticker, particleSystem);
		movementModule = new ParticleMovementModule();

		emitter.define(movementModule);
		ticker.update(0);
	});

	context("When setting the `spawnRegion`", ()=>
	{
		it("particles start in that defined region.", ()=>
		{
			movementModule.spawnRegion(new Vector2SpawnRegion(new Vector2(10,20)));

			const particle = emitter.emitParticle();

			Assert.strictEqual((<ParticleMovementComponent>particle.components[0]).position.x, 10);
			Assert.strictEqual((<ParticleMovementComponent>particle.components[0]).position.y, 20);
			Assert.strictEqual(particle.sprite.x, 10);
			Assert.strictEqual(particle.sprite.y, 20);
		});

		it("to undefined the particle positions default to 0.", ()=>
		{
			const particle = emitter.emitParticle();

			Assert.strictEqual(particle.sprite.x, 0);
			Assert.strictEqual(particle.sprite.y, 0);
		});
	});

	context("When setting a `velocity`", ()=>
	{
		it("the particles positions update by their velocity.", () =>
		{
			movementModule.velocity(new Vector2(10, 5));

			const particle = emitter.emitParticle();

			ticker.update(1);
			Assert.strictEqual(particle.sprite.x, 10);
			Assert.strictEqual(particle.sprite.y, 5);

			ticker.update(3);
			Assert.strictEqual(particle.sprite.x, 30);
			Assert.strictEqual(particle.sprite.y, 15);
		});

		it("the velocity is set to a random value within the given range for each particle.", () =>
		{
			movementModule.velocityRange(new Vector2(-5, -5), new Vector2(5, 5));

			TestUtils.fakeRandomReturnValues(0,0,0,0, 0.1,0.8,0,0, 0.5,0.5,0,0, 0.9,0.9,0,0);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle1.components[0]).velocity.x, -5);
			Assert.strictEqual((<ParticleMovementComponent>particle1.components[0]).velocity.y, -5);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle2.components[0]).velocity.x, -4);
			Assert.strictEqual((<ParticleMovementComponent>particle2.components[0]).velocity.y, 3);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle3.components[0]).velocity.x, 0);
			Assert.strictEqual((<ParticleMovementComponent>particle3.components[0]).velocity.y, 0);

			const particle4 = emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle4.components[0]).velocity.x, 4);
			Assert.strictEqual((<ParticleMovementComponent>particle4.components[0]).velocity.y, 4);
		});
	});

	context("When setting an `acceleration`", ()=>
	{
		it("the velocity updates by the acceleration before updating the position.", ()=>
		{
			movementModule.acceleration(new Vector2(1, 2));

			const particle = emitter.emitParticle();

			ticker.update(1);
			Assert.strictEqual(particle.sprite.x, 1);
			Assert.strictEqual(particle.sprite.y, 2);

			ticker.update(2);
			Assert.strictEqual(particle.sprite.x, 3);
			Assert.strictEqual(particle.sprite.y, 6);

			ticker.update(3);
			Assert.strictEqual(particle.sprite.x, 6);
			Assert.strictEqual(particle.sprite.y, 12);
		});

		it("the acceleration is set to a random value within the given range for each particle.", () =>
		{
			movementModule.accelerationRange(new Vector2(-5, -5), new Vector2(5, 5));

			TestUtils.fakeRandomReturnValues(0,0,0,0, 0,0,0.1,0.8, 0,0,0.5,0.5, 0,0,0.9,0.9);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle1.components[0]).acceleration.x, -5);
			Assert.strictEqual((<ParticleMovementComponent>particle1.components[0]).acceleration.y, -5);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle2.components[0]).acceleration.x, -4);
			Assert.strictEqual((<ParticleMovementComponent>particle2.components[0]).acceleration.y, 3);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle3.components[0]).acceleration.x, 0);
			Assert.strictEqual((<ParticleMovementComponent>particle3.components[0]).acceleration.y, 0);

			const particle4 = emitter.emitParticle();
			Assert.strictEqual((<ParticleMovementComponent>particle4.components[0]).acceleration.x, 4);
			Assert.strictEqual((<ParticleMovementComponent>particle4.components[0]).acceleration.y, 4);
		});
	});
});