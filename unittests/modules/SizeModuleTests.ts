import {it} from "mocha";
import {Ticker} from "pixi.js";
import {
	Particle,
	ParticleEmitter,
	ParticleSizeComponent,
	ParticleSizeModule,
	ParticleSystem
} from "../../src";
import {Vector2} from "@worldweaver/math";
import * as Assert from "assert";
import TestUtils from "../TestUtils";

describe("SizeModule", ()=>
{
	let ticker: Ticker;
	let particleSystem: ParticleSystem;
	let emitter: ParticleEmitter;
	let sizeModule: ParticleSizeModule;

	beforeEach(()=>
	{
		ticker = new Ticker();
		particleSystem = new ParticleSystem(ticker);
		emitter = new ParticleEmitter(ticker, particleSystem);
		sizeModule = new ParticleSizeModule();

		emitter.define(sizeModule);
		ticker.update(0);
	});

	context("When setting a `size`", ()=>
	{
		it("the sprite size is changed to match it when the component is added.", ()=>
		{
			sizeModule.size(new Vector2(99,42));

			const particle = emitter.emitParticle();

			Assert.strictEqual(particle.sprite.width, 99);
			Assert.strictEqual(particle.sprite.height, 42);
		});

		it("the sizeVelocity increases by the sizeAcceleration each tick.", ()=>
		{
			sizeModule
				.sizeVelocity(new Vector2(0, 0))
				.sizeAcceleration(new Vector2(10, 5));

			const particle = emitter.emitParticle();

			ticker.update(1);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).sizeVelocity.x, 10);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).sizeVelocity.y, 5);

			ticker.update(3);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).sizeVelocity.x, 30);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).sizeVelocity.y, 15);
		});

		it("the size increases by the sizeVelocity each tick.", ()=>
		{
			sizeModule.sizeVelocity(new Vector2(10, 5));

			const particle = emitter.emitParticle();

			ticker.update(1);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).size.x, 10);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).size.y, 5);
			Assert.strictEqual(particle.sprite.width, 10);
			Assert.strictEqual(particle.sprite.height, 5);

			ticker.update(3);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).size.x, 30);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).size.y, 15);
			Assert.strictEqual(particle.sprite.width, 30);
			Assert.strictEqual(particle.sprite.height, 15);
		});

		it("the size is selected from the given range at random.", ()=>
		{
			sizeModule.sizeRange(new Vector2(0, 0), new Vector2(10, 10));

			TestUtils.fakeRandomReturnValues(
				0,0, 0,0, 0,0, 0, 0, 0,
				0.1,0.8, 0,0, 0,0, 0, 0, 0,
				0.9,0.9, 0,0, 0,0, 0, 0, 0
			);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).size.x, 0);
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).size.y, 0);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).size.x, 1);
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).size.y, 8);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).size.x, 9);
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).size.y, 9);
		});

		it("the sizeVelocity is selected from the given range at random.", ()=>
		{
			sizeModule.sizeVelocityRange(new Vector2(0, 0), new Vector2(10, 10));

			TestUtils.fakeRandomReturnValues(
				0,0, 0,0, 0,0, 0, 0, 0,
				0,0, 0.1,0.8, 0,0, 0, 0, 0,
				0,0, 0.9,0.9, 0,0, 0, 0, 0
			);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).sizeVelocity.x, 0);
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).sizeVelocity.y, 0);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).sizeVelocity.x, 1);
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).sizeVelocity.y, 8);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).sizeVelocity.x, 9);
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).sizeVelocity.y, 9);
		});

		it("the sizeAcceleration is selected from the given range at random.", ()=>
		{
			sizeModule.sizeAccelerationRange(new Vector2(0, 0), new Vector2(10, 10));

			TestUtils.fakeRandomReturnValues(
				0,0, 0,0, 0,0, 0, 0, 0,
				0,0, 0,0, 0.1,0.8, 0, 0, 0,
				0,0, 0,0, 0.9,0.9, 0, 0, 0
			);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).sizeAcceleration.x, 0);
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).sizeAcceleration.y, 0);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).sizeAcceleration.x, 1);
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).sizeAcceleration.y, 8);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).sizeAcceleration.x, 9);
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).sizeAcceleration.y, 9);
		});
	});

	context("When setting a `scale`", ()=>
	{
		it("the sprite size is changed to match the scale when the component is added.", ()=>
		{
			sizeModule.size(new Vector2(10,5)).scale(3);

			const particle = emitter.emitParticle();

			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).size.x, 10);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).size.y, 5);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).scale, 3);
			Assert.strictEqual(particle.sprite.width, 30);
			Assert.strictEqual(particle.sprite.height, 15);
		});

		it("the scaleVelocity increases by the scaleAcceleration each tick.", ()=>
		{
			sizeModule
				.scaleVelocity(0)
				.scaleAcceleration(10);

			const particle = emitter.emitParticle();

			ticker.update(1);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).scaleVelocity, 10);

			ticker.update(3);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).scaleVelocity, 30);
		});

		it("the scale increases by the scaleVelocity each tick.", ()=>
		{
			sizeModule.size(new Vector2(10,5)).scaleVelocity(2);

			const particle = emitter.emitParticle();

			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).scale, 1);
			Assert.strictEqual(particle.sprite.width, 10);
			Assert.strictEqual(particle.sprite.height, 5);

			ticker.update(1);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).scale, 3);
			Assert.strictEqual(particle.sprite.width, 30);
			Assert.strictEqual(particle.sprite.height, 15);

			ticker.update(3);
			Assert.strictEqual((<ParticleSizeComponent>particle.components[0]).scale, 7);
			Assert.strictEqual(particle.sprite.width, 70);
			Assert.strictEqual(particle.sprite.height, 35);
		});

		it("the scale is selected from the given range at random.", ()=>
		{
			sizeModule.scaleRange(0, 10);

			TestUtils.fakeRandomReturnValues(
				0,0, 0,0, 0,0, 0, 0, 0,
				0,0, 0,0, 0,0, 0.5, 0, 0,
				0,0, 0,0, 0,0, 0.9, 0, 0,
			);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).scale, 0);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).scale, 5);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).scale, 9);
		});

		it("the scaleVelocity is selected from the given range at random.", ()=>
		{
			sizeModule.scaleVelocityRange(0, 10);

			TestUtils.fakeRandomReturnValues(
				0,0, 0,0, 0,0, 0, 0, 0,
				0,0, 0,0, 0,0, 0, 0.5, 0,
				0,0, 0,0, 0,0, 0, 0.9, 0,
			);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).scaleVelocity, 0);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).scaleVelocity, 5);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).scaleVelocity, 9);
		});

		it("the sizeAcceleration is selected from the given range at random.", ()=>
		{
			sizeModule.scaleAccelerationRange(0, 10);

			TestUtils.fakeRandomReturnValues(
				0,0, 0,0, 0,0, 0, 0, 0,
				0,0, 0,0, 0,0, 0, 0, 0.5,
				0,0, 0,0, 0,0, 0, 0, 0.9,
			);

			const particle1 = <Particle>emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle1.components[0]).scaleAcceleration, 0);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle2.components[0]).scaleAcceleration, 5);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual((<ParticleSizeComponent>particle3.components[0]).scaleAcceleration, 9);
		});
	});
});