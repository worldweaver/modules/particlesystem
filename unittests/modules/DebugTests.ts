import {it} from "mocha";
import {Ticker} from "pixi.js";
import {ParticleEmitter, ParticleSystem} from "../../src";
import {ParticleDebugCounterModule} from "../../src/modules/debug";
import * as Assert from "assert";
import MockParticleDisposer from "../mock/MockParticleDisposer";

describe("DebugModules", ()=>
{
	let ticker: Ticker;
	let particleSystem: ParticleSystem;
	let emitter: ParticleEmitter;

	beforeEach(()=>
	{
		ticker = new Ticker();
		particleSystem = new ParticleSystem(ticker);
		emitter = new ParticleEmitter(ticker, particleSystem);
		ticker.update(0);
	});

	context("When adding a `ParticleDebugCounterModule`", ()=>
	{
		let particleCounter: ParticleDebugCounterModule;
		beforeEach(()=>
		{
			particleCounter = new ParticleDebugCounterModule();
			emitter.define(particleCounter);
		});

		it("the particle count increases for every particle.", () =>
		{
			Assert.strictEqual(particleCounter.particleCount, 0);

			emitter.emitParticle();
			Assert.strictEqual(particleCounter.particleCount, 1);

			emitter.batchCount(3).emitBatch();
			Assert.strictEqual(particleCounter.particleCount, 4);
		});

		it("the particle count decreases for every particle that is disposed.", () =>
		{
			const disposer = new MockParticleDisposer();
			emitter.define(disposer);
			Assert.strictEqual(particleCounter.particleCount, 0);

			emitter.batchCount(3).emitBatch();
			Assert.strictEqual(particleCounter.particleCount, 3);

			disposer.disposeParticle();
			Assert.strictEqual(particleCounter.particleCount, 2);

			disposer.disposeParticle();
			Assert.strictEqual(particleCounter.particleCount, 1);

			disposer.disposeParticle();
			Assert.strictEqual(particleCounter.particleCount, 0);
		});
	});
});