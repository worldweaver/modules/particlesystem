import {Texture, Ticker} from "pixi.js";
import {ParticleEmitter, ParticleStaticImageModule, ParticleSystem} from "../../src";
import TestUtils from "../TestUtils";
import * as Assert from "assert";

describe("StaticImageModule", ()=>
{
	let ticker: Ticker;
	let particleSystem: ParticleSystem;
	let emitter: ParticleEmitter;
	let staticImageModule: ParticleStaticImageModule;

	beforeEach(()=>
	{
		ticker = new Ticker();
		particleSystem = new ParticleSystem(ticker);
		emitter = new ParticleEmitter(ticker, particleSystem);
		staticImageModule = new ParticleStaticImageModule();

		emitter.define(staticImageModule);
		ticker.update(0);
	});

	context("When setting a texture", ()=>
	{
		it("the particles sprite is set to the given texture",()=>
		{
			const texture = Texture.from("../textures/4x4particle.jpg");

			staticImageModule.texture(texture);

			const particle = emitter.emitParticle();

			Assert.strictEqual(particle.sprite.texture, texture);
		});

		it("the particles are given a texture at random from the array provided",()=>
		{
			const texture1 = Texture.from("../textures/4x4particle.jpg");
			const texture2 = texture1.clone();
			const texture3 = texture1.clone();

			staticImageModule.textures(texture1, texture2, texture3);

			TestUtils.fakeRandomReturnValues(0.5, 0, 0.9);

			const particle1 = emitter.emitParticle();
			Assert.strictEqual(particle1.sprite.texture, texture2);

			const particle2 = emitter.emitParticle();
			Assert.strictEqual(particle2.sprite.texture, texture1);

			const particle3 = emitter.emitParticle();
			Assert.strictEqual(particle3.sprite.texture, texture3);
		});
	});


	context("When no texture is set", ()=>
	{
		it("an error is thrown.", ()=>
		{
			Assert.throws(()=>
			{
				emitter.emitParticle();
			});
		});
	});
});