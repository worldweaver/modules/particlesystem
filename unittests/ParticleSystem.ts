import * as Assert from "assert";
import {it} from "mocha";
import {ParticleSystem} from "../src";
import {Ticker} from "pixi.js";
import MockParticleTickTracker from "./mock/MockParticleTickTracker";

describe("ParticleSystem", ()=>
{
	context("When using `getParticle`", () =>
	{
		it("the first particle created is assigned as the root and last particle", () =>
		{
			const particleSystem = new ParticleSystem(new Ticker());
			const particle = particleSystem.createParticle();

			// @ts-ignore
			Assert.strictEqual(particle, particleSystem._rootParticle);
			// @ts-ignore
			Assert.strictEqual(particle, particleSystem._lastParticle);
		});

		it("the last particle created is assigned as the last particle", () =>
		{
			const particleSystem = new ParticleSystem(new Ticker());
			const firstParticle = particleSystem.createParticle();
			const lastParticle = particleSystem.createParticle();

			// @ts-ignore
			Assert.strictEqual(firstParticle, particleSystem._rootParticle);
			// @ts-ignore
			Assert.notStrictEqual(firstParticle, particleSystem._lastParticle);

			// @ts-ignore
			Assert.notStrictEqual(lastParticle, particleSystem._rootParticle);
			// @ts-ignore
			Assert.strictEqual(lastParticle, particleSystem._lastParticle);
		});
	});

	context("When the Ticker ticks", () =>
	{
		it("the ParticleSystem ticks all particles", () =>
		{
			const ticker = new Ticker();
			ticker.update(0);

			const particleSystem = new ParticleSystem(ticker);
			const particleComponents = [];

			for(let i=0; i<100; i++)
			{
				const particleComponent = new MockParticleTickTracker();
				const particle = particleSystem.createParticle();
				particle.add(particleComponent);
				particleComponents.push(particleComponent);
			}

			ticker.update(10);

			for(let i=0; i<particleComponents.length; i++)
			{
				Assert.strictEqual(particleComponents[i].ticked, 10);
			}
		});
	});
});
