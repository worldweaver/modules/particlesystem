export default class TestUtils
{
	private static _random = Math.random;

	static fakeRandomReturnValues(...randomValues: Array<number>): void
	{
		let randomIndex = 0;
		Math.random = () =>
		{
			if(randomIndex >= randomValues.length)
			{
				Math.random = TestUtils._random;
				return Math.random();
			}
			return randomValues[randomIndex++];
		}
	}
}