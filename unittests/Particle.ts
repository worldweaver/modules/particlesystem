import {it} from "mocha";
import {Particle, ParticleLifeSpanComponent, ParticleMovementComponent} from "../src";
import * as Assert from "assert";
import MockDisposableParticleComponent from "./mock/MockDisposableParticleComponent";
import MockParticleTickTracker from "./mock/MockParticleTickTracker";
import {Vector2} from "@worldweaver/math";

describe("Particle", ()=>
{
	context("When `disposing`", () =>
	{
		it("the next and previous particles are linked", () =>
		{
			const particleA = new Particle();
			const particleB = new Particle();
			const particleC = new Particle();

			particleA.next = particleB;
			particleB.prev = particleA;

			particleB.next = particleC;
			particleC.prev = particleB;

			particleB.dispose();

			Assert.strictEqual(particleA.next, particleC);
			Assert.strictEqual(particleC.prev, particleA);
		});

		it("the components are disposed", () =>
		{
			const particle = new Particle();
			const mockDisposable = new MockDisposableParticleComponent();
			particle.add(mockDisposable);

			Assert.strictEqual(mockDisposable.disposed, false);

			particle.dispose();

			Assert.strictEqual(mockDisposable.disposed, true);
		});

		it("the sprite is disposed", () =>
		{
			const particle = new Particle();

			Assert.strictEqual(particle.sprite.destroyed, false);

			particle.dispose();

			Assert.strictEqual(particle.sprite.destroyed, true);
		});

		it("the components stop ticking", () =>
		{
			const particle = new Particle();
			const tickTracker = new MockParticleTickTracker();

			particle.add(new ParticleLifeSpanComponent(10));
			particle.add(tickTracker);
			particle.add(new ParticleMovementComponent(new Vector2(), new Vector2(), new Vector2()));

			particle.tick(9);

			Assert.strictEqual(tickTracker.ticked, 9);

			Assert.doesNotThrow(()=>
			{
				particle.tick(1);
			});

			Assert.strictEqual(tickTracker.ticked, 9);
		});
	});
});