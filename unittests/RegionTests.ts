import * as Assert from "assert";
import {it} from "mocha";
import RectSpawnRegion from "../src/regions/RectSpawnRegion";
import {Rectangle} from "pixi.js";
import TestUtils from "./TestUtils";

describe("Region", ()=>
{
	context("When using a RectSpawnRegion", () =>
	{
		it("the default rect returns 0,0 as the position", () =>
		{
			const rectRegion = new RectSpawnRegion();
			rectRegion.determinePosition();

			for(let i=0; i<3; i++)
			{
				const result = rectRegion.determinePosition();
				Assert.strictEqual(result.x, 0);
				Assert.strictEqual(result.y, 0);
			}
		});

		it("the determined position should be within the width and height of the rect", () =>
		{
			const rect = new Rectangle(0, 0, 10, 20);
			const rectRegion = new RectSpawnRegion(rect);

			for(let i=0; i<1000; i++)
			{
				const result = rectRegion.determinePosition();
				Assert.strictEqual(result.x < rect.width, true);
				Assert.strictEqual(result.x >= rect.x, true);
				Assert.strictEqual(result.y < rect.height, true);
				Assert.strictEqual(result.y >= rect.y, true);
			}
		});
		it("the result should be consistent with the random values used", () =>
		{
			TestUtils.fakeRandomReturnValues(0.1, 0.2, 0.3, 0.4, 0.5, 0.5);

			const rect = new Rectangle(0, 0, 10, 20);
			const rectRegion = new RectSpawnRegion(rect);

			let result = rectRegion.determinePosition();

			Assert.strictEqual(result.x, 1);
			Assert.strictEqual(result.y, 4);

			result = rectRegion.determinePosition();

			Assert.strictEqual(result.x, 3);
			Assert.strictEqual(result.y, 8);

			result = rectRegion.determinePosition();

			Assert.strictEqual(result.x, 5);
			Assert.strictEqual(result.y, 10);
		});

		it("the determined values should account for the position of the rect", () =>
		{
			TestUtils.fakeRandomReturnValues(0.5, 0.5);

			const rect = new Rectangle(5, 10, 10, 20);
			const rectRegion = new RectSpawnRegion(rect);

			const result = rectRegion.determinePosition();

			Assert.strictEqual(result.x, 10);
			Assert.strictEqual(result.y, 20);
		});
	});
});