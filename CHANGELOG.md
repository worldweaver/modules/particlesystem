## 1.1.1

* Fixed an issue with disposing a particle while still iterating through its components.

## 1.1.0

* Added full unit tests for existing functionality.
* Added a debug module that provides a way of counting particles.
* Added the ability for particle components to react to the particle being disposed.
* Added various getters for better accessibility.
* Fixed a variety of small bugs.
* Updated to Math 1.1.1.

# 1.0.0

* Initial POC
* Support for basic static image particles.
* Support for a AABB rectangular spawn region.
* Support for defining/randomising the lifespan, size, scale and movement of a particle.