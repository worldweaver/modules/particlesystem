import IParticleComponent from "./IParticleComponent";
import {Sprite} from "pixi.js";

export default interface IParticle
{
	get components(): IParticleComponent[];
	get sprite(): Sprite;

	get prev(): IParticle | undefined;
	set prev(particle: IParticle | undefined);
	get next(): IParticle | undefined;
	set next(particle: IParticle | undefined);

	add(component: IParticleComponent): IParticle;
	tick(elapsedTime: number): void;
	dispose(): void;
}