import IParticle from "./IParticle";

export default interface IParticleComponent
{
	init(particle: IParticle): void;
	tick(elapsedTime: number, particle: IParticle): void;
	dispose(): void;
}