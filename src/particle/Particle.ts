import IParticle from "./IParticle";
import IParticleComponent from "./IParticleComponent";
import {Sprite} from "pixi.js";

export default class Particle implements IParticle
{
	private readonly _components: IParticleComponent[];

	private readonly _sprite: Sprite;

	private _next: IParticle | undefined;
	private _prev: IParticle | undefined;

	private _disposed: boolean;

	get components()
	{
		return this._components;
	}

	get sprite()
	{
		return this._sprite;
	}

	get prev(): IParticle | undefined
	{
		return this._prev;
	}
	set prev(value: IParticle | undefined)
	{
		this._prev = value;
	}

	get next(): IParticle | undefined
	{
		return this._next;
	}
	set next(value: IParticle | undefined)
	{
		this._next = value;
	}

	constructor()
	{
		this._components = [];
		this._sprite = new Sprite();
		this._sprite.anchor.x = 0.5;
		this._sprite.anchor.y = 0.5;
		this._disposed = false;
	}

	add(component: IParticleComponent): IParticle
	{
		this._components.push(component);
		component.init(this);
		return this;
	}

	tick(elapsedTime: number): void
	{
		for (const c of this._components)
		{
			if(this._disposed) break;

			c.tick(elapsedTime, this);
		}
	}

	dispose(): void
	{
		this._components.forEach(components=>components.dispose());
		this._sprite.destroy();

		if(this._prev != undefined)
		{
			this._prev.next = this._next;
		}
		if(this._next != undefined)
		{
			this._next.prev = this._prev;
		}

		this._disposed = true;
	}
}