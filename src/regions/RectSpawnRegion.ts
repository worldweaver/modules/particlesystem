import {Rectangle} from "pixi.js";
import IParticleSpawnRegion from "./IParticleSpawnRegion";
import {Vector2} from "@worldweaver/math";

export default class RectSpawnRegion implements IParticleSpawnRegion
{
	private _rect: Rectangle;

	constructor(rect = new Rectangle())
	{
		this._rect = rect;
	}

	determinePosition(): Vector2
	{
		return new Vector2(
			this._rect.x + (Math.random() * this._rect.width),
			this._rect.y + (Math.random() * this._rect.height)
		);
	}
}