import {Vector2} from "@worldweaver/math";

export default interface IParticleSpawnRegion
{
	determinePosition() : Vector2;
}