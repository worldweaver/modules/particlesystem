import IParticleSpawnRegion from "./IParticleSpawnRegion";
import {Vector2} from "@worldweaver/math";

export default class Vector2SpawnRegion implements IParticleSpawnRegion
{
	private readonly _position: Vector2;

	constructor(position: Vector2)
	{
		this._position = position;
	}

	determinePosition(): Vector2
	{
		return this._position;
	}
}