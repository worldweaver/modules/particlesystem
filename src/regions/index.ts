import IParticleSpawnRegion from "./IParticleSpawnRegion";
import RectSpawnRegion from "./RectSpawnRegion";

export {
	IParticleSpawnRegion,
	RectSpawnRegion
}