import "reflect-metadata";
import {inject, injectable} from "inversify";
import {ParticleContainer, Ticker} from "pixi.js";
import IParticleModule from "./IParticleModule";
import ParticleSystem from "./ParticleSystem";
import {PixiDI} from "@worldweaver/pixi-integration";
import {IParticle} from "./particle";

@injectable()
export default class ParticleEmitter
{
	private _ticker: Ticker;
	private _particleSystem: ParticleSystem;
	private _particleContainer!: ParticleContainer;

	private _isRunning: boolean;
	private _batchCount: number;
	private _emitRate: number;
	private _emitTimeRemainder: number;

	private _particleModules: Array<IParticleModule>;

	constructor(
		@inject(PixiDI.Ticker) ticker: Ticker,
		@inject(ParticleSystem) particleSystem: ParticleSystem
	)
	{
		this._ticker = ticker;
		this._particleSystem = particleSystem;

		this._particleModules = new Array<IParticleModule>();
		this._isRunning = false;
		this._batchCount = 1;
		this._emitRate = 1;
		this._emitTimeRemainder = 0;

		ticker.add(elapsedTime=>
		{
			this.tick(this._ticker.elapsedMS);
		});
	}

	public container(container: ParticleContainer) : ParticleEmitter
	{
		this._particleContainer = container;
		return this;
	}

	public batchCount(batchCount: number) : ParticleEmitter
	{
		this._batchCount = batchCount;
		return this;
	}
	public emitRate(emitRate: number) : ParticleEmitter
	{
		this._emitRate = emitRate;
		return this;
	}

	public define(module: IParticleModule) : ParticleEmitter
	{
		this._particleModules.push(module);
		return this;
	}

	public start(elapsedTime: number = 0): void
	{
		this._emitTimeRemainder = elapsedTime;
		this._isRunning = true;
	}

	public stop(): void
	{
		this._isRunning = false;
		this._emitTimeRemainder = 0;
	}

	tick(elapsedTime: number): void
	{
		if (!this._isRunning || (elapsedTime === 0 && this._emitTimeRemainder === 0)) return;

		let timeToAccount = this._emitTimeRemainder + elapsedTime;

		while(timeToAccount>=this._emitRate)
		{
			timeToAccount -= this._emitRate;

			this.emitBatch(timeToAccount);
		}

		this._emitTimeRemainder = timeToAccount;
	}

	public emitBatch(elapsedTime: number = 0): IParticle[]
	{
		const particles = [];

		for(let i=0; i<this._batchCount; i++)
		{
			particles.push(this.emitParticle(elapsedTime));
		}

		return particles;
	}

	public emitParticle(elapsedTime: number = 0): IParticle
	{
		const particle = this._particleSystem.createParticle();

		this._particleContainer?.addChild(particle.sprite);

		this._particleModules.forEach(m=>
		{
			particle.add(
				m.createComponent(particle)
			);
		});

		particle.tick(elapsedTime);

		return particle;
	}
}