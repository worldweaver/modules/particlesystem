import IParticle from "./particle/IParticle";
import {inject, injectable} from "inversify";
import {Ticker} from "pixi.js";
import Particle from "./particle/Particle";
import {PixiDI} from "@worldweaver/pixi-integration";

@injectable()
export default class ParticleSystem
{
	private readonly _ticker: Ticker;
	private _rootParticle: Particle | undefined;
	private _lastParticle: Particle | undefined;

	constructor(
		@inject(PixiDI.Ticker) ticker: Ticker
	)
	{
		this._ticker = ticker;
		ticker.add(elapsedTime=>
		{
			this.tick(this._ticker.elapsedMS)
		});
	}

	public createParticle() : Particle
	{
		const particle = new Particle();

		if(this._rootParticle == undefined)
		{
			this._rootParticle = particle;
		}
		else
		{
			particle.prev = this._lastParticle;
		}
		if(this._lastParticle != undefined)
		{
			this._lastParticle.next = particle;
		}
		this._lastParticle = particle;

		return particle;
	}

	tick(elapsedTime: number) : void
	{
		let particle: IParticle | undefined = this._rootParticle;

		while(particle != undefined)
		{
			particle.tick(elapsedTime);

			particle = particle.next;
		}
	}
}