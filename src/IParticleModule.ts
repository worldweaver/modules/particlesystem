import IParticleComponent from "./particle/IParticleComponent";
import IParticle from "./particle/IParticle";

export default interface IParticleModule
{
	createComponent(particle: IParticle): IParticleComponent;
}