export * from "./regions";
export * from "./particle";
export * from "./modules";

import IParticleModule from "./IParticleModule";
import ParticlesDI from "./ParticlesDI";
import ParticleEmitter from "./ParticleEmitter";
import ParticleSystem from "./ParticleSystem";

export {
	IParticleModule,
	ParticlesDI,
	ParticleEmitter,
	ParticleSystem
}