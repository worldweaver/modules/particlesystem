import IParticleComponent from "../../particle/IParticleComponent";
import {IReadOnlyVector2, Vector2} from "@worldweaver/math";
import IParticle from "../../particle/IParticle";

export default class ParticleSizeComponent implements IParticleComponent
{
	private readonly _size: Vector2;
	private readonly _sizeVelocity: Vector2;
	private readonly _sizeAcceleration: Vector2;

	private _scale: number;
	private _scaleVelocity: number;
	private readonly _scaleAcceleration: number;

	get size(): IReadOnlyVector2
	{
		return this._size;
	}
	get sizeVelocity(): IReadOnlyVector2
	{
		return this._sizeVelocity;
	}
	get sizeAcceleration(): IReadOnlyVector2
	{
		return this._sizeAcceleration;
	}

	get scale(): number
	{
		return this._scale;
	}
	get scaleVelocity(): number
	{
		return this._scaleVelocity;
	}
	get scaleAcceleration(): number
	{
		return this._scaleAcceleration;
	}

	constructor(
		size: Vector2,
		sizeVelocity: Vector2,
		sizeAcceleration: Vector2,
		scale: number,
		scaleVelocity: number,
		scaleAcceleration: number)
	{
		this._size = size;
		this._sizeVelocity = sizeVelocity;
		this._sizeAcceleration = sizeAcceleration;
		this._scale = scale;
		this._scaleVelocity = scaleVelocity;
		this._scaleAcceleration = scaleAcceleration;
	}

	init(particle: IParticle): void
	{
		particle.sprite.width = this._size.x * this._scale;
		particle.sprite.height = this._size.y * this._scale;
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
		const scaleAccelerationThisTick = this._scaleAcceleration * elapsedTime;
		this._scaleVelocity += scaleAccelerationThisTick;

		const scaleVelocityThisTick = this._scaleVelocity * elapsedTime;
		this._scale += scaleVelocityThisTick;

		const sizeAccelerationThisTick = this._sizeAcceleration.multiplyScalar(elapsedTime);
		this._sizeVelocity.addToThis(sizeAccelerationThisTick);

		const sizeVelocityThisTick = this._sizeVelocity.multiplyScalar(elapsedTime);
		this._size.addToThis(sizeVelocityThisTick);

		particle.sprite.width = this._size.x * this._scale;
		particle.sprite.height = this._size.y * this._scale;
	}

	dispose(): void
	{
	}
}