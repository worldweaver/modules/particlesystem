import IParticleModule from "../../IParticleModule";
import IParticle from "../../particle/IParticle";
import IParticleComponent from "../../particle/IParticleComponent";
import {Vector2, Range, Number} from "@worldweaver/math";
import ParticleSizeComponent from "./ParticleSizeComponent";

export default class ParticleSizeModule implements IParticleModule
{
	private _sizeRange: Range<Vector2>;
	private _sizeVelocityRange: Range<Vector2>;
	private _sizeAccelerationRange: Range<Vector2>;

	private _scaleRange: Range<Number>;
	private _scaleVelocityRange: Range<Number>;
	private _scaleAccelerationRange: Range<Number>;

	constructor()
	{
		this._sizeRange = new Range<Vector2>(new Vector2());
		this._sizeVelocityRange = new Range<Vector2>(new Vector2());
		this._sizeAccelerationRange = new Range<Vector2>(new Vector2());

		this._scaleRange = new Range<Number>(new Number(1));
		this._scaleVelocityRange = new Range<Number>(new Number(0));
		this._scaleAccelerationRange = new Range<Number>(new Number(0));
	}

	public size(value: Vector2) : ParticleSizeModule
	{
		this._sizeRange = new Range<Vector2>(value, value);
		return this;
	}
	public sizeRange(minValue: Vector2, maxValue: Vector2) : ParticleSizeModule
	{
		this._sizeRange.set(minValue, maxValue);
		return this;
	}

	public sizeVelocity(velocity: Vector2) : ParticleSizeModule
	{
		this._sizeVelocityRange = new Range<Vector2>(velocity, velocity);
		return this;
	}
	public sizeVelocityRange(minVelocity: Vector2, maxVelocity: Vector2) : ParticleSizeModule
	{
		this._sizeVelocityRange.set(minVelocity, maxVelocity);
		return this;
	}

	public sizeAcceleration(acceleration: Vector2) : ParticleSizeModule
	{
		this._sizeAccelerationRange = new Range<Vector2>(acceleration, acceleration);
		return this;
	}
	public sizeAccelerationRange(minAcceleration: Vector2, maxAcceleration: Vector2) : ParticleSizeModule
	{
		this._sizeAccelerationRange.set(minAcceleration, maxAcceleration);
		return this;
	}

	public scale(scale: number) : ParticleSizeModule
	{
		this._scaleRange.min.value = scale;
		this._scaleRange.max.value = scale;
		return this;
	}
	public scaleRange(minScale: number, maxScale: number) : ParticleSizeModule
	{
		this._scaleRange.min.value = minScale;
		this._scaleRange.max.value = maxScale;
		return this;
	}

	public scaleVelocity(velocity: number) : ParticleSizeModule
	{
		this._scaleVelocityRange.min.value = velocity;
		this._scaleVelocityRange.max.value = velocity;
		return this;
	}
	public scaleVelocityRange(minVelocity: number, maxVelocity: number) : ParticleSizeModule
	{
		this._scaleVelocityRange.min.value = minVelocity;
		this._scaleVelocityRange.max.value = maxVelocity;
		return this;
	}

	public scaleAcceleration(acceleration: number) : ParticleSizeModule
	{
		this._scaleAccelerationRange.min.value = acceleration;
		this._scaleAccelerationRange.max.value = acceleration;
		return this;
	}
	public scaleAccelerationRange(minAcceleration: number, maxAcceleration: number) : ParticleSizeModule
	{
		this._scaleAccelerationRange.min.value = minAcceleration;
		this._scaleAccelerationRange.max.value = maxAcceleration;
		return this;
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		return new ParticleSizeComponent(
			this._sizeRange.random(),
			this._sizeVelocityRange.random(),
			this._sizeAccelerationRange.random(),

			this._scaleRange.random().value,
			this._scaleVelocityRange.random().value,
			this._scaleAccelerationRange.random().value
		);
	}
}