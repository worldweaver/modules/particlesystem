import ParticleStaticImageModule from "./ParticleStaticImageModule";
import ParticleStaticImageComponent from "./ParticleStaticImageComponent";

export {
	ParticleStaticImageModule,
	ParticleStaticImageComponent
}