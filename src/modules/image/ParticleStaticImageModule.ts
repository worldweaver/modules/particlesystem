import {Texture} from "pixi.js";
import IParticle from "../../particle/IParticle";
import ParticleStaticImageComponent from "./ParticleStaticImageComponent";
import {Range, Number} from "@worldweaver/math";
import IParticleModule from "../../IParticleModule";
import IParticleComponent from "../../particle/IParticleComponent";

export default class ParticleStaticImageModule implements IParticleModule
{
	private _textures: Texture[] | undefined;
	private _range: Range<Number> = new Range<Number>(new Number(0));

	public textures(...value: Texture[]) : ParticleStaticImageModule
	{
		this._textures = value;
		this._range.max.value = value.length;
		return this;
	}

	public texture(value: Texture) : ParticleStaticImageModule
	{
		this._textures = new Array<Texture>(value);
		this._range.max.value = 0;
		return this;
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		if(this._textures == undefined)
		{
			throw new Error("ParticleStaticImageModule textures must be assigned.");
		}

		return new ParticleStaticImageComponent(
			this._textures[Math.floor(this._range.random().value)]
		);
	}
}