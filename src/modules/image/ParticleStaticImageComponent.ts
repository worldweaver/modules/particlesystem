import {Texture} from "pixi.js";
import IParticleComponent from "../../particle/IParticleComponent";
import IParticle from "../../particle/IParticle";

export default class ParticleStaticImageComponent implements IParticleComponent
{
	private readonly _texture: Texture;

	constructor(texture: Texture)
	{
		this._texture = texture;
	}

	init(particle: IParticle): void
	{
		particle.sprite.texture = this._texture;
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
		// no need to tick as the texture is set already
	}

	dispose(): void
	{
	}
}