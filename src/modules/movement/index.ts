import ParticleMovementModule from "./ParticleMovementModule";
import ParticleMovementComponent from "./ParticleMovementComponent";

export {
	ParticleMovementModule,
	ParticleMovementComponent
}