import IParticleComponent from "../../particle/IParticleComponent";
import {IReadOnlyVector2, Vector2} from "@worldweaver/math";
import IParticle from "../../particle/IParticle";

export default class ParticleMovementComponent implements IParticleComponent
{
	private readonly _position: Vector2;
	private readonly _velocity: Vector2;
	private readonly _acceleration: Vector2;

	get position(): IReadOnlyVector2
	{
		return this._position;
	}

	get velocity(): IReadOnlyVector2
	{
		return this._velocity;
	}

	get acceleration(): IReadOnlyVector2
	{
		return this._acceleration;
	}

	constructor(position: Vector2, velocity: Vector2, acceleration: Vector2)
	{
		this._position = position;
		this._velocity = velocity;
		this._acceleration = acceleration;
	}

	init(particle: IParticle): void
	{
		particle.sprite.x = this._position.x;
		particle.sprite.y = this._position.y;
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
		const accelerationThisTick = this._acceleration.multiplyScalar(elapsedTime);
		this._velocity.addToThis(accelerationThisTick);

		const velocityThisTick = this._velocity.multiplyScalar(elapsedTime);
		this._position.addToThis(velocityThisTick);

		particle.sprite.x = this._position.x;
		particle.sprite.y = this._position.y;
	}

	dispose(): void
	{
	}
}