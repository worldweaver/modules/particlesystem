import IParticleSpawnRegion from "../../regions/IParticleSpawnRegion";
import IParticleModule from "../../IParticleModule";
import {Vector2, Range} from "@worldweaver/math";
import IParticle from "../../particle/IParticle";
import IParticleComponent from "../../particle/IParticleComponent";
import ParticleMovementComponent from "./ParticleMovementComponent";
import Vector2SpawnRegion from "../../regions/Vector2SpawnRegion";

export default class ParticleMovementModule implements IParticleModule
{
	private _spawnRegion: IParticleSpawnRegion;
	private _velocityRange: Range<Vector2>;
	private _accelerationRange: Range<Vector2>;

	public spawnRegion(value: IParticleSpawnRegion) : ParticleMovementModule
	{
		this._spawnRegion = value;
		return this;
	}

	public velocity(velocity: Vector2) : ParticleMovementModule
	{
		this._velocityRange = new Range<Vector2>(velocity, velocity);
		return this;
	}
	public velocityRange(minVelocity: Vector2, maxVelocity: Vector2) : ParticleMovementModule
	{
		this._velocityRange = new Range<Vector2>(minVelocity, maxVelocity);
		return this;
	}

	public acceleration(acceleration: Vector2) : ParticleMovementModule
	{
		this._accelerationRange = new Range<Vector2>(acceleration, acceleration);
		return this;
	}
	public accelerationRange(minAcceleration: Vector2, maxAcceleration: Vector2) : ParticleMovementModule
	{
		this._accelerationRange = new Range<Vector2>(minAcceleration, maxAcceleration);
		return this;
	}

	constructor()
	{
		this._velocityRange = new Range<Vector2>(new Vector2(), new Vector2());
		this._accelerationRange = new Range<Vector2>(new Vector2(), new Vector2());
		this._spawnRegion = new Vector2SpawnRegion(new Vector2(0, 0));
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		return new ParticleMovementComponent(
			this._spawnRegion.determinePosition(),
			this._velocityRange.random(),
			this._accelerationRange.random()
		);
	}
}