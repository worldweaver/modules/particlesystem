import IParticleModule from "../../IParticleModule";
import IParticle from "../../particle/IParticle";
import IParticleComponent from "../../particle/IParticleComponent";
import {Range, Number} from "@worldweaver/math";
import ParticleLifeSpanComponent from "./ParticleLifeSpanComponent";

export default class ParticleLifeSpanModule implements IParticleModule
{
	private _lifeSpanRange: Range<Number>;

	public lifespan(lifespan: number) : ParticleLifeSpanModule
	{
		this._lifeSpanRange.min.value = lifespan;
		this._lifeSpanRange.max.value = lifespan;
		return this;
	}

	public lifespanRange(minLifespan: number, maxLifespan: number) : ParticleLifeSpanModule
	{
		this._lifeSpanRange.min.value = minLifespan;
		this._lifeSpanRange.max.value = maxLifespan;
		return this;
	}

	constructor()
	{
		this._lifeSpanRange = new Range<Number>(new Number(0));
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		return new ParticleLifeSpanComponent(this._lifeSpanRange.random().value);
	}
}