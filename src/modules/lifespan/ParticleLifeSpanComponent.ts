import IParticleComponent from "../../particle/IParticleComponent";
import IParticle from "../../particle/IParticle";

export default class ParticleLifeSpanComponent implements IParticleComponent
{
	private _lifeSpanRemaining: number;

	get lifeSpan(): number
	{
		return this._lifeSpanRemaining;
	}

	constructor(lifeSpan: number)
	{
		this._lifeSpanRemaining = lifeSpan;
	}

	init(particle: IParticle): void
	{
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
		this._lifeSpanRemaining -= elapsedTime;

		if(this._lifeSpanRemaining <= 0)
		{
			particle.dispose();
		}
	}

	dispose(): void
	{
	}
}