import ParticleLifeSpanModule from "./ParticleLifeSpanModule";
import ParticleLifeSpanComponent from "./ParticleLifeSpanComponent";

export {
	ParticleLifeSpanModule,
	ParticleLifeSpanComponent
}