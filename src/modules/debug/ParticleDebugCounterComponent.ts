import {IParticle, IParticleComponent} from "../../particle";
import ParticleDebugCounterModule from "./ParticleDebugCounterModule";

export default class ParticleDebugCounterComponent implements IParticleComponent
{
	private _countModule: ParticleDebugCounterModule;

	constructor(countModule: ParticleDebugCounterModule)
	{
		this._countModule = countModule;
	}

	init(particle: IParticle): void
	{
		this._countModule.increment();
	}

	tick(elapsedTime: number, particle: IParticle): void
	{
	}

	dispose(): void
	{
		this._countModule.decrement();
	}
}