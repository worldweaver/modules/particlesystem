import IParticleModule from "../../IParticleModule";
import {IParticle, IParticleComponent} from "../../particle";
import ParticleDebugCounterComponent from "./ParticleDebugCounterComponent";

export default class ParticleDebugCounterModule implements IParticleModule
{
	private _particleCount: number = 0;

	public get particleCount(): number
	{
		return this._particleCount;
	}

	public increment(): void
	{
		this._particleCount ++;
	}

	public decrement(): void
	{
		this._particleCount --;
	}

	createComponent(particle: IParticle): IParticleComponent
	{
		return new ParticleDebugCounterComponent(this);
	}
}