export default {
	ParticleSystem: Symbol.for("ParticleSystem"),
	ParticleEmitter: Symbol.for("ParticleEmitter")
}